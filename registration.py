import os
from urllib.parse import quote_plus

from envparse import Env
from yaml import load, dump
import json
from gitlab import Gitlab as GitLab
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

env = Env()
if os.path.isfile('.env'):
    env.read_envfile()


def determine_environment(gl, need, environment_name, strategy):

    strategies = {x: None for x in strategy}

    for environment in gl.projects.get(quote_plus(need)).environments.list(all=True):
        print(environment.name, environment.external_url)
        if '$CI_ENVIRONMENT_NAME' in strategies and environment.name == environment_name:
            strategies['$CI_ENVIRONMENT_NAME'] = environment.external_url
        elif 'staging' in strategies and environment.name == 'staging':
            strategies['staging'] = environment.external_url
        elif 'production' in strategies and environment.name == 'production':
            strategies['production'] = environment.external_url
    return next(iter([x for x in strategies.values() if x])) or ''


def main():
    """
    This is used to ensure that a all services are registered
    """
    # needs = env('UPSTREAM_SERVICE_NEEDS', cast=list, default=[])
    project_id = env('UPSTREAM_PROJECT_ID', cast=int, default=-1)
    project_name = env('UPSTREAM_PROJECT_PATH', cast=str, default='')
    environment_name = env('UPSTREAM_ENVIRONMENT_NAME', cast=str, default='')
    environment_url = env('UPSTREAM_ENVIRONMENT_URL', cast=str, default='')

    private_token = env('PRIVATE_TOKEN')
    gl = GitLab(env('CI_SERVER_URL'), private_token=private_token)
    gl.auth()

    print('Project Name:', project_name)
    print('Environment Name:', environment_name)
    print('Environment URL:', environment_url)

    commit_info = {
        'branch': 'master',
        'commit_message': 'Saving Copy',
        'actions': []
    }

    with open('registry.yml') as f:
        data = load(f.read(), Loader=Loader)

    if project_name and environment_url:
        for project in data.get('projects', {}):
            if project.get('name') == project_name or project_id == project.get('id'):
                project['name'] = project_name
                project['id'] = project_id
                if not isinstance(project.get('needs', None), list):
                    project['needs'] = []
                if not isinstance(project['urls'], dict):
                    project['dict'] = {}
                project['urls'][environment_name] = environment_url
                print(f'Setting url ({environment_name}) to {environment_url}')

        with open('registry.yml', 'w') as f:
            f.write(dump(data))
        commit_info['actions'].append({
            'action': 'update',
            'file_path': 'registry.yml',
            'content': dump(data),
        })
        commit = gl.projects.get(env('CI_PROJECT_ID')).commits.create(commit_info)
    with open('registry.json', 'w') as f:
        f.write(json.dumps(data))
    # commit_info['actions'].append({
    #     'action': 'update',
    #     'file_path': 'registry.json',
    #     'content': json.dumps(data),
    # })



if __name__ == '__main__':
    main()
