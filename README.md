# Service Registry

* `.gitlab-ci.yml`: Used to both build registry service, add additional projects to give them the ability to register new environments

## Getting Started:

**Creating A Service Registry File**

In this project `registry.yml`:

The following will be used to keep track of which environments are available.  This particular use case is going to provide
each consumer the ability to grab a file and parse it within the Auto DevOps job and save those URLs as Kubernetes
Secrets.

```yaml
projects:
- id: 1569
  name: demosys-users/tpoffenbarger/microservices-demo/pact-demo
  needs: []
  secret: PACT_DEMO_URL
  urls:
    production: http://pact-demo.demo.ten08.com
    review/tpoffenbarger-master-patch-56137: http://1569-review-tpoffenbar-slju01.demo.ten08.com
    staging: http://pact-demo-staging.demo.ten08.com
- id: 154
  name: demosys-users/tpoffenbarger/django-auto-devops
  needs:
  - demosys-users/tpoffenbarger/microservices-demo/pact-demo
  secret: DJANGO_AUTO_DEVOPS_URL
  urls:
    production: http://django-autodevops.demo.ten08.com
- id: 1575
  name: demosys-users/tpoffenbarger/microservices-demo/pact-provider
  needs:
  - demosys-users/tpoffenbarger/microservices-demo/pact-demo
  - demosys-users/tpoffenbarger/django-auto-devops
  secret: PACT_PROVIDER_URL
settings:
  strategy:
  - CI_ENVIRONMENT_NAME
  - staging
  - production

```


In Other Projects: `.gitlab-ci.yml`:

```yaml
include:
# - template: Auto-DevOps.gitlab-ci.yml
- project: demosys-users/tpoffenbarger/microservices-demo/service-registry
  ref: master
  file: Auto-DevOps.gitlab-ci.yml
```
